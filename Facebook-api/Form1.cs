﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Facebook;
using System.IO;
using System.Net;
using System.Web;

//Ryan Guarascia
//June, 18, 2014
//Post status and photo to Facebook, remotely
namespace Facebook_api
{
    public partial class Form1 : Form
    {

        const long myId = 42;
        const string appId = "Your Application ID";
        const string appSecret = "You Application Secret";
        const string clientSecret = "Your Application Client Secret";
        string token = "You Token, only valid for 3-5 hours. Must regen";
        const string callBack = "https://github.com/rguarascia";
        public Form1()
        {
            InitializeComponent();
        }

        private void btnPost_Click(object sender, EventArgs e)
        {
            postWall(richTextBox1.Text, myId, token);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var fbApp = new FacebookClient(token);
            string _filename = "";
            OpenFileDialog myImg = new OpenFileDialog();
            if (myImg.ShowDialog() == DialogResult.OK)
                _filename = myImg.FileName;

            bool ver = PostImage(token, richTextBox1.Text, _filename);
            MessageBox.Show(ver.ToString());
        }
        private static void postWall(string message, long userId, string wallAccessToken)
        {
            try
            {
                var fb = new FacebookClient(wallAccessToken);
                string url = string.Format("{0}/{1}", userId, "feed");
                var argList = new Dictionary<string, object>();
                argList["message"] = message;
                fb.Post(url, argList);
                MessageBox.Show("True");
            } 
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        public static bool PostImage(string AccessToken, string Status, string ImagePath)
        {
            try
            {
                FacebookClient fb = new FacebookClient(AccessToken);
                var imgstream = File.OpenRead(ImagePath);
                dynamic res = fb.Post("/me/photos", new
                {
                    message = Status,
                    file = new FacebookMediaStream
                    {
                        ContentType = "image/jpg",
                        FileName = Path.GetFileName(ImagePath)
                    }.SetValue(imgstream)
                });
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }
    }
}
